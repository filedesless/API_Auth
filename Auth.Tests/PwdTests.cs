using Auth.Lib;
using Xunit;

namespace Auth.Tests
{
    public class PwdTests
    {
        [Theory]
        [InlineData("")]
        [InlineData("test")]
        [InlineData("SomeGoodPassword123!")]
        public void HashPasswordShouldReturnANonEmptyString(string pass)
        {
            var res = Pwd.HashPassword(pass);
            Assert.False(string.IsNullOrWhiteSpace(res));
        }

        [Theory]
        [InlineData("")]
        [InlineData("asdf")]
        [InlineData("SomeGoodPass512?")]
        public void VerifyPasswordShouldValidateCorrectPassword(string pass)
        {
            var hash = Pwd.HashPassword(pass);
            var res = Pwd.VerifyPassword(hash, pass);

            Assert.True(res);
        }

        [Theory]
        [InlineData("")]
        [InlineData("asdf")]
        [InlineData("SomeGoodPass512?")]
        public void VerifyPasswordShouldNotValidateIncorrectPassword(string pass)
        {
            var hash = Pwd.HashPassword(pass);
            var res = Pwd.VerifyPassword(hash, "IncorrectPass");

            Assert.False(res);
        }
    }
}