FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /build

# copy everything and build app
COPY ./ ./
RUN dotnet build -c Release \
 && dotnet test -c Release --no-build ./Auth.Tests \
 && dotnet publish -c Release -o /release ./Auth

FROM microsoft/dotnet:2.1-aspnetcore-runtime AS runtime
WORKDIR /app
COPY --from=build /release ./
ENTRYPOINT ["dotnet", "Auth.dll"]
