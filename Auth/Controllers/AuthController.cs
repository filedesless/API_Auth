using System;
using System.Linq;
using System.Threading.Tasks;
using Auth.Lib;
using Auth.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Auth.Controllers
{
    public class NewUser
    {
        public string Email;
        public string Password;
        public string Username;
    }

    public class KnownUser
    {
        public string Password;
        public string Username;
    }

    public class NewName
    {
        public string SessionId;
        public string Username;
    }

    public class NewPass
    {
        public string SessionId;
        public string Password;
    }

    [Route("[controller]/")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly AuthContext _ctx;

        public AuthController(AuthContext ctx)
        {
            _ctx = ctx;
            _ctx.RemoveRange(_ctx.Sessions.Where(s => s.Expiration <= DateTime.Now));
            _ctx.SaveChanges();
        }

        // Creates account
        [HttpPost("[action]")]
        public async Task<ApiResponse> Introduce(NewUser user)
        {
            var response = new ApiResponse();

            if (string.IsNullOrWhiteSpace(user.Username))
                response.Errors.Add("User is empty");

            if (string.IsNullOrWhiteSpace(user.Email))
                response.Errors.Add("Email is empty");

            if (await _ctx.Users.AnyAsync(u => u.Username == user.Username))
                response.Errors.Add("Username is already taken");

            if (await _ctx.Users.AnyAsync(u => u.Email == user.Email))
                response.Errors.Add("Email is already taken");

            if (user.Password.Length < 8)
                response.Errors.Add("Password must be at least 8 characters long");

            if (!user.Password.Any(char.IsUpper))
                response.Errors.Add("Password must contain at least 1 uppercase character");

            if (!user.Password.Any(char.IsLower))
                response.Errors.Add("Password must contain at least 1 lowercase character");

            if (user.Password.All(char.IsLetter))
                response.Errors.Add("Password must contain at least 1 non letter character");

            response.Success = response.Errors.Count == 0;

            if (!response.Success)
                return response;

            var newUser = new User
            {
                Username = user.Username,
                Email = user.Email,
                PasswordHash = Pwd.HashPassword(user.Password)
            };

            await _ctx.Users.AddAsync(newUser);
            await _ctx.SaveChangesAsync();
            response.Message = newUser.Id;

            return response;
        }

        // Creates session
        [HttpPost("[action]")]
        public async Task<ApiResponse> Authenticate(KnownUser knownUser)
        {
            var user = await _ctx.Users.FirstOrDefaultAsync(u => u.Username == knownUser.Username);
            if (user == null)
                return new ApiResponse
                {
                    Success = false,
                    Errors = {"Invalid credentials"}
                };

            if (!Pwd.VerifyPassword(user.PasswordHash, knownUser.Password))
                return new ApiResponse
                {
                    Success = false,
                    Errors = {"Invalid credentials"}
                };

            string sesId;
            do
            {
                sesId = Ses.GenerateRandomSesId();
            } while (await _ctx.Sessions.AnyAsync(s => s.SesId == sesId));

            var ses = new Session
            {
                UserId = user.Id,
                SesId = sesId,
                Expiration = DateTime.Now.AddDays(2)
            };

            await _ctx.Sessions.AddAsync(ses);
            await _ctx.SaveChangesAsync();

            return new ApiResponse
            {
                Success = true,
                Message = ses.SesId
            };
        }

        // Verify session
        [HttpPost("[action]")]
        public async Task<ApiResponse> Identify([FromBody] string sid)
        {
            var session = await _ctx.Sessions.FirstOrDefaultAsync(s => s.SesId == sid);
            if (session == null)
                return new ApiResponse
                {
                    Success = false,
                    Errors = {"Inexistant sessionId"}
                };

            return new ApiResponse
            {
                Success = true,
                Message = session.User.Username
            };
        }

        // Delete user
        [HttpPost("Self/[action]")]
        public async Task<ApiResponse> Erase([FromBody] string sid)
        {
            var session = await _ctx.Sessions.FirstOrDefaultAsync(s => s.SesId == sid);
            if (session == null)
                return new ApiResponse
                {
                    Success = false,
                    Errors = {"Inexistant sessionId"}
                };

            var userId = session.User.Id;
            _ctx.Users.Remove(session.User);
            await _ctx.SaveChangesAsync();

            return new ApiResponse
            {
                Success = true,
                Message = userId
            };
        }

        // Delete session
        [HttpPost("Self/[action]")]
        public async Task<ApiResponse> Invalidate([FromBody] string sid)
        {
            var session = await _ctx.Sessions.FirstOrDefaultAsync(s => s.SesId == sid);
            if (session == null)
                return new ApiResponse
                {
                    Success = false,
                    Errors = {"Inexistant sessionId"}
                };

            _ctx.Sessions.Remove(session);
            await _ctx.SaveChangesAsync();

            return new ApiResponse
            {
                Success = true
            };
        }

        // Delete all user's sessions
        [HttpPost("Self/[action]")]
        public async Task<ApiResponse> Resiliate([FromBody] string sid)
        {
            var session = await _ctx.Sessions.FirstOrDefaultAsync(s => s.SesId == sid);
            if (session == null)
                return new ApiResponse
                {
                    Success = false,
                    Errors = {"Inexistant sessionId"}
                };

            var userId = session.UserId;
            _ctx.Sessions.RemoveRange(_ctx.Sessions.Where(s => s.UserId == session.UserId));
            await _ctx.SaveChangesAsync();

            return new ApiResponse
            {
                Success = true,
                Message = userId
            };
        }

        [HttpPost("Self/[action]")]
        public async Task<ApiResponse> Rename(NewName newName)
        {
            if (string.IsNullOrWhiteSpace(newName.Username))
                return new ApiResponse
                {
                    Success = false,
                    Errors = {"Username is empty"}
                };

            if (await _ctx.Users.AnyAsync(u => u.Username == newName.Username))
                return new ApiResponse
                {
                    Success = false,
                    Errors = {"Username is already taken"}
                };

            var session = await _ctx.Sessions.FirstOrDefaultAsync(s => s.SesId == newName.SessionId);
            if (session == null)
                return new ApiResponse
                {
                    Success = false,
                    Errors = {"Inexistant sessionId"}
                };

            var user = await _ctx.Users.FindAsync(session.UserId);
            user.Username = newName.Username;
            await _ctx.SaveChangesAsync();

            return new ApiResponse
            {
                Success = true,
                Message = user.Id
            };
        }

        [HttpPost("Self/[action]")]
        public async Task<ApiResponse> ChangePass(NewPass newPass)
        {
            var response = new ApiResponse();
            if (newPass.Password.Length < 8)
                response.Errors.Add("Password must be at least 8 characters long");

            if (!newPass.Password.Any(char.IsUpper))
                response.Errors.Add("Password must contain at least 1 uppercase character");

            if (!newPass.Password.Any(char.IsLower))
                response.Errors.Add("Password must contain at least 1 lowercase character");

            if (newPass.Password.All(char.IsLetter))
                response.Errors.Add("Password must contain at least 1 non letter character");

            var session = await _ctx.Sessions.FirstOrDefaultAsync(s => s.SesId == newPass.SessionId);
            if (session == null)
                response.Errors.Add("Inexistant sessionId");
            
            response.Success = response.Errors.Count == 0;

            if (response.Success)
                response.Message = session.UserId;

            return response;
        }
    }
}