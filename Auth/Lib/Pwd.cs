using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace Auth.Lib
{
    public class Algo
    {
        public Func<string, byte[], string> Hasher; // password: string -> salt: byte[] -> hash: string
        public int HashSize;
        public int SaltSize;
    }

    public static class Pwd
    {
        private const int CurrentVersion = 0x1;

        private static Dictionary<int, Algo> Algorithms => new Dictionary<int, Algo>
        {
            {0x1, new Algo {SaltSize = 128 / 8, HashSize = 256 / 8, Hasher = HashPasswordV1}}
        };

        public static string HashPassword(string password, int version = CurrentVersion)
        {
            return HashPassword(password, GetRandomSalt(Algorithms[version].SaltSize));
        }

        public static bool VerifyPassword(string hash, string password)
        {
            var src = Convert.FromBase64String(hash);
            int version = src[0];
            var algo = Algorithms[version];
            var salt = new byte[algo.SaltSize];
            Buffer.BlockCopy(src, 1, salt, 0, algo.SaltSize);

            return HashPassword(password, salt, version) == hash;
        }

        // generate a 128-bit salt using a secure PRNG
        private static byte[] GetRandomSalt(int size)
        {
            var salt = new byte[size];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            return salt;
        }

        private static string HashPassword(string password, byte[] salt, int version = CurrentVersion)
        {
            return Algorithms[version].Hasher(password, salt);
        }

        // derive a 256-bit subkey (use HMACSHA512 with 10,000 iterations)
        private static string HashPasswordV1(string password, byte[] salt)
        {
            var algo = Algorithms[0x1];
            var hash = KeyDerivation.Pbkdf2(password, salt, KeyDerivationPrf.HMACSHA512, 10000, algo.HashSize);

            var output = new byte[1 + algo.SaltSize + algo.HashSize];
            output[0] = CurrentVersion;
            Buffer.BlockCopy(salt, 0, output, 1, algo.SaltSize);
            Buffer.BlockCopy(hash, 0, output, 1 + algo.SaltSize, algo.HashSize);

            return Convert.ToBase64String(output);
        }
    }
}