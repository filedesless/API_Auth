using System;
using Microsoft.EntityFrameworkCore;

namespace Auth.Models
{
    public class AuthContext : DbContext
    {
        public AuthContext(DbContextOptions options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Session> Sessions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .ForSqlServerIsMemoryOptimized();

            modelBuilder.Entity<Session>()
                .ForSqlServerIsMemoryOptimized();

            
            modelBuilder.Entity<User>()
                .HasIndex(p => p.Email)
                .IsUnique();

            modelBuilder.Entity<User>()
                .HasIndex(u => u.Username)
                .IsUnique();

            modelBuilder.Entity<Session>()
                .HasOne(s => s.User)
                .WithMany(u => u.Sessions)
                .HasForeignKey(s => s.UserId);

            modelBuilder.Entity<Session>()
                .HasIndex(s => s.SesId)
                .IsUnique();
        }
    }
}